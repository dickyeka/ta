from django.contrib import admin

# Register your models here.


from .models import Employer,Job,AttendanceList,ScanLog


admin.site.register(Employer)
admin.site.register(AttendanceList)
admin.site.register(Job)
admin.site.register(ScanLog)