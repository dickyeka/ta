from django.core.management.base import BaseCommand, CommandError
import requests
from .models import ScanLog


class Command(BaseCommand):
    help = 'Syc Scan Log'
    
    def handle(self, *args, **options):
        response = requests.post('http://192.168.0.123:8080',data = {'sn' : '00225016240043'})
        json_response = response.json()
        
        data = json_response['data']

        for d in data : 
            scan = ScanLog(sn=d['SN'],scan_date=d['ScanDate'],pin=d['PIN'],cekin=d['IN'],cekout=d['OUT'])
            