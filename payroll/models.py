from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from django.db.models import signals
from django.dispatch import receiver
from django.contrib.auth.models import AbstractUser


class Employer(models.Model) :

    GENDER_CHOICES  = (
        ('M','Male'),
        ('F','Female')
    )

    STATUS_PAJAK  = (
        ('k0','k0'),
        ('k1','k1'),
        ('k2','k2'),
        ('k3','k3'),
        ('tk0','tk0'),
        ('tk1','tk1'),
        ('tk2','tk2'),
        ('tk3','tk3')
    )

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    gender = models.CharField(max_length=2,choices=GENDER_CHOICES)
    no_ktp = models.CharField(max_length=20)
    born_date = models.DateField()
    bron_place = models.CharField(max_length=100)
    phone = models.CharField(max_length=15)
    address = models.TextField()
    email = models.EmailField()
    last_education = models.CharField(max_length=200)
    educational_institution = models.CharField(max_length=200)
    major = models.CharField(max_length=200)
    no_npwp = models.CharField(max_length=50)
    status_pajak = models.CharField(max_length=10,choices=STATUS_PAJAK)
    no_bank = models.CharField(max_length=50)
    name_bank = models.CharField(max_length=100)
    account_name_bank = models.CharField(max_length=200)    
    photo = models.FileField(max_length=200)
    pin=models.CharField(max_length=20)
    tgl_kerja = models.DateField(default=datetime.now, blank=True)

    @property
    def position(self):
        return self.job_set.filter(active=True).first()


    @property
    def jobCount(self):
        return self.job_set.count()


    def __str__(self):
        return self.name


# @receiver(signals.pre_save, sender=Employer)
# def create_employer(sender, **kwargs):
    
#     print kwargs['instance'].pk
#     user = User.objects.create_user(
#             email=kwargs['instance'].email,
#             username=kwargs['instance'].email,
#             password="123456",
#             is_employer=True,
#             employer_id =  kwargs['instance'].pk
#         )


class User(AbstractUser):
    is_employer = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    employer_id = models.IntegerField()



class Job(models.Model) :
    id = models.AutoField(primary_key=True)
    position = models.CharField(max_length=200)
    employer = models.ForeignKey(Employer,on_delete=models.CASCADE)
    salary = models.IntegerField()
    start_date = models.DateField()
    active = models.BooleanField()

    
    def __str__(self):
       return self.position



class AttendanceList(models.Model) :

    id = models.AutoField(primary_key=True)
    date = models.DateTimeField()
    employer = models.ForeignKey(Employer,on_delete=models.CASCADE)
    status = models.CharField(max_length=20)


class Allowance(models.Model)  :

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    amount = models.IntegerField(null=True)


class SalarySlip(models.Model) :

    id = models.AutoField(primary_key=True)
    employer = models.ForeignKey(Employer,on_delete=models.CASCADE)
    date = models.DateTimeField()
    payment_status = models.BooleanField(default=False)
    total_income = models.IntegerField()
    total_deduction = models.IntegerField()
    month = models.IntegerField()
    year = models.IntegerField()
    total_pay = models.IntegerField()

    @property
    def income(self):
        return self.income_set.all()

    @property
    def deduction(self):
        return self.deduction_set.all()



class Income(models.Model) :
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    unit = models.IntegerField()
    total = models.IntegerField()
    salaryslip = models.ForeignKey(SalarySlip,on_delete=models.CASCADE)

class Deduction(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    unit = models.IntegerField()
    total = models.IntegerField()
    salaryslip = models.ForeignKey(SalarySlip,on_delete=models.CASCADE)


class Cuti(models.Model) :
    id = models.AutoField(primary_key=True)
    employer = models.ForeignKey(Employer,on_delete=models.CASCADE)
    start = models.DateTimeField()
    end = models.DateTimeField()
    quota = models.IntegerField()
    take = models.IntegerField()



class ScanLog(models.Model) :
    id = models.AutoField(primary_key=True)
    sn=models.CharField(max_length=200)
    scan_date =models.DateTimeField()
    pin=models.CharField(max_length=10)
    verifymode=models.IntegerField()
    iomode=models.IntegerField()
    workcode=models.IntegerField()
    cekin=models.CharField(max_length=10)
    cekout=models.CharField(max_length=10)


class Holidays(models.Model) :
    id = models.AutoField(primary_key=True)
    date = models.DateField()
    name = models.CharField(max_length=200)


class StatusPajak(models.Model) :
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    amount = models.IntegerField()


class Izin(models.Model) :

    GENDER_CHOICES  = (
        ('I','Izin'),
        ('S','Sakit')
    )
    id = models.AutoField(primary_key=True)
    type = models.CharField(max_length=2,choices=GENDER_CHOICES)
    date = models.CharField(max_length=200)
    keterangan = models.CharField(max_length=200)
    surat = models.FileField(upload_to='documents/',default=None, blank=True, null=True)
    status = models.BooleanField(default=False)
    employer = models.ForeignKey(Employer,on_delete=models.CASCADE)


class Option(models.Model) :
     id = models.AutoField(primary_key=True)
     cuti = models.ImageField()
     

class JatahCuti(models.Model) :
    id = models.AutoField(primary_key=True)
    employer = models.ForeignKey(Employer,on_delete=models.CASCADE)
    start = models.DateField()
    end = models.DateField()
    quota = models.IntegerField()
    take = models.IntegerField()

    @property
    def sisa(self):
        return self.quota-self.take



class PengajuanCuti(models.Model) :
    id = models.AutoField(primary_key=True)
    date = models.CharField(max_length=200)
    keterangan = models.CharField(max_length=200)
    status = models.BooleanField(default=False)
    employer = models.ForeignKey(Employer,on_delete=models.CASCADE)


