from django.shortcuts import render
from datetime import datetime
from .models import Employer,Job,AttendanceList,SalarySlip,Allowance,JatahCuti,ScanLog,Holidays,StatusPajak,Income,Deduction,Izin,PengajuanCuti,User
from .forms  import EmployerForm,JobForm,AllowanceForm,IzinForm,CutiForm,LoginForm
from django.views.generic import CreateView, UpdateView, DetailView, DeleteView, ListView,View
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from datetime import date
from django.urls import reverse
import calendar
from dateutil.rrule import rrule, DAILY
from django.shortcuts import redirect
from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages
from decimal import Decimal

def index(request) : 
    
    today = date.today()
    start_date = date(today.year,today.month,1)
    end_date = date(today.year,today.month,calendar.mdays[today.month])


    countEmpy = Employer.objects.count()
    
    countIzin = AttendanceList.objects.filter(date__range=(start_date,end_date),status='I').count()
    countSakit = AttendanceList.objects.filter(date__range=(start_date,end_date),status='S').count()
    countCuti =  AttendanceList.objects.filter(date__range=(start_date,end_date),status='Cuti').count()

    employer = Employer.objects.all()
    totalHadir =  0
    totalSakit = 0
    totalIzin = 0
    totalCuti = 0
    jumlahHariKerja = jamKerja()


    for e in employer :         
        jumlahHadir = ScanLog.objects.filter(pin=e.id,scan_date__range=(start_date,end_date)).values('scan_date').distinct().count()     
        jumlahSakit = AttendanceList.objects.filter(employer_id=e.id,status="S",date__range=(start_date,end_date)).values('date').distinct().count()
        jumlahIzin = AttendanceList.objects.filter(employer_id=e.id,status="I",date__range=(start_date,end_date)).values('date').distinct().count() 
        jumalhCuti = AttendanceList.objects.filter(employer_id=e.id,status="Cuti",date__range=(start_date,end_date)).values('date').distinct().count()     
        totalHadir +=jumlahHadir
        if(jumlahSakit) :
            totalSakit+=jumlahSakit
        if(jumlahSakit) :
            totalIzin+=jumlahIzin
        if(jumalhCuti) :
            totalCuti+=jumalhCuti

    totalHariKerja = float(jumlahHariKerja*countEmpy)
    persentaseHadir = (totalHadir/totalHariKerja)*100
    persentaseSakit = (totalSakit/totalHariKerja)*100
    persentaseIzin = (totalIzin/totalHariKerja)*100
    persentaseCuti = (totalCuti/totalHariKerja)*100
    persentaseTidakMasuk = 100-(persentaseHadir+persentaseSakit+persentaseIzin+persentaseCuti)

    TWOPLACES = Decimal(10) ** -2 

    context = {
        'countEmpy' : countEmpy,
        'countIzin' : countIzin,
        'countSakit' : countSakit,
        'countCuti' : countCuti,
        'persentaseHadir' : Decimal(persentaseHadir).quantize(TWOPLACES),
        'persentaseSakit' : Decimal(persentaseSakit).quantize(TWOPLACES),
        'persentaseIzin' : Decimal(persentaseIzin).quantize(TWOPLACES),
        'persentaseCuti' : Decimal(persentaseCuti).quantize(TWOPLACES),
        'persentaseTidakMasuk' : Decimal(persentaseTidakMasuk).quantize(TWOPLACES),
        'title' : 'Home'
    }

    return render(request,'index.html',context)



class ViewListEmployer(ListView):
    queryset = Employer.objects.all()
    paginate_by = 10
    template_name = 'employer/list.html'



class ViewCreateEmployer(CreateView):
    template_name = 'employer/create.html'
    model = Employer
    form_class = EmployerForm
    success_url = '/employer'


    def form_valid(self, form):
        
        instance = form.save()

        employer_id =  instance.id
        user = User.objects.create_user(
            email=instance.email,
            username=instance.email,
            password="123456",
            is_employer=True,
            employer_id =  employer_id
        )
        
        return super(ViewCreateEmployer,self).form_valid(form)

   

class ViewUpdateEmployer(UpdateView):

    model = Employer
    template_name = 'employer/update.html'
    form_class = EmployerForm
    success_url = '/employer'

    def get_object(self, queryset=None):
        id = self.kwargs['id']
        return self.model.objects.get(id=id)

    

class ViewDetailEmployer(DetailView):
    model = Employer
    template_name = 'employer/detail.html'


def DeleteEmploye(request, pk):
    employer = Employer.objects.get(pk=pk)
    employer.delete()
    return JsonResponse({'status':'deleted'})


# Job

class ViewListJob(ListView):
    model = Job
    paginate_by = 10
    template_name = 'job/list.html'


    def get_queryset(self):
        return Job.objects.filter(employer__pk=self.kwargs['pk']).order_by('-active')


    def get_context_data(self, **kwargs):
        context = super(ViewListJob, self).get_context_data(**kwargs)
        context['employer_id'] = self.kwargs['pk']
        return context

  


class CreateViewEmployerJobCreate(CreateView) :
    model = Job
    form_class = JobForm
    template_name =  'job/create.html';
    success_url = '/employer'
    
    def form_valid(self,form) :
        count = Job.objects.filter(employer__pk=self.kwargs['class']).count()

        if(count>1) :
            jobs = Job.objects.filter(employer__pk=self.kwargs['class'])
            for job in jobs:
                job.active = False;
                job.save()
        
        instance = form.save(commit=False)
        instance.employer = Employer.objects.get(pk=self.kwargs['class'])
        instance.active = True;

        return super(CreateViewEmployerJobCreate,self).form_valid(form)


class ViewUpdateJob(UpdateView):

    model = Job
    template_name = 'job/update.html'
    form_class = JobForm
    

    def get_success_url(self):
        id = self.kwargs['id']
        job =self.model.objects.get(id=id) 
        return reverse('job-list', kwargs={'pk':job.employer_id})

    def get_object(self, queryset=None):
        id = self.kwargs['id']
        return self.model.objects.get(id=id)


class ViewListAttendance(ListView):
    
    model = AttendanceList
    paginate_by = 10
    template_name = 'attendance/list.html'


    def get_queryset(self):
        filter_val = self.request.GET.get('filter',1)
        new_context = AttendanceList.objects.filter(
            employer_id=filter_val,
        )
        return new_context

    def get_context_data(self, **kwargs):
        context = super(ViewListAttendance, self).get_context_data(**kwargs)
        context['filter'] = self.request.GET.get('filter',1)
        return context



def list_kehadiran(request) :
    
    if request.GET.has_key('filter'):
        today = request.GET.get('filter')
    else :
        today = date.today()
    
    scanlog =   ScanLog.objects.filter(scan_date__contains=today);
    
    employer = Employer.objects.all()

    data = []

    for e in employer :
        scan = ScanLog.objects.filter(scan_date__contains=today,pin=e.pin).first()

        row = {}
        row['name'] = e.name
        row['pin'] = e.pin

        if scan:
            row['status'] = ''
            row['in'] = scan.cekin
            row['out'] = scan.cekout 

        else :
            izin = AttendanceList.objects.filter(employer_id=e.id,date__contains=today).first()
               
            if(izin) :                
                if izin.status == "I" :
                    row['status'] = "Izin"
                elif izin.status == "S" :
                    row['status'] = "Sakit"
                else:
                    row['status'] = "Cuti"
            else :
                row['status'] = 'Tidak Masuk'
            
            row['in'] = '-'
            row['out'] = '-'
            
        data.append(row)

    return render(request, 'attendance/list.html', { 'data': data })



#rekap kehadrian 

def rekap_kehadiran(request) :

  
    if request.GET.has_key('month'):
        today = date.today()
        month = int(request.GET.get('month'))
        start_date = date(today.year,month,1)
        end_date = date(today.year,month,calendar.mdays[month])
        selectMonth = month

    else :
        today = date.today()
        start_date = date(today.year,today.month,1)
        end_date = date(today.year,today.month,calendar.mdays[today.month])
        selectMonth = today.month


    mounth  = [];
    for i in range(1, 13):
        mounth.append(calendar.month_name[i]);

    employer = Employer.objects.all();

    data = []

    for e in employer :
        row = {}
        jumlahHadir = ScanLog.objects.filter(pin=e.pin,scan_date__range=(start_date,end_date)).values('scan_date').distinct().count
        jumlahSakit = AttendanceList.objects.filter(employer_id=e.id,status="S",date__range=(start_date,end_date)).values('date').distinct().count
        jumlahIzin = AttendanceList.objects.filter(employer_id=e.id,status="I",date__range=(start_date,end_date)).values('date').distinct().count
        row['name'] = e.name
        row['jumlah_hadir'] = jumlahHadir
        row['jumlah_sakit'] = jumlahSakit
        row['jumlah_izin'] = jumlahIzin
        row['id'] = e.id
        data.append(row)
    

    return render(request, 'attendance/rekap-kehadiran.html',{'mounth':mounth,'data':data,'selectMonth' : selectMonth})


def jamKerja() :

    jumlah=0
    
    today = date.today()
    month = today.month
    start_date = date(today.year,today.month,1)
    end_date = date(today.year,today.month,calendar.mdays[month])

    #hari libur
    holidays  = Holidays.objects.filter(date__month=month,date__year=today.year)
    listDay = []


    for dt in rrule(DAILY,dtstart=start_date,until=end_date): 
        
        dayName =  dt.strftime("%a")
        d = dt.strftime("%d")
        if dayName == 'Sat' or dayName == 'Sun' :
            pass
        elif  d  in listDay :
            pass
        else :
            jumlah+=1


    return jumlah


def user_kehadiran(request) :

    month = int(request.GET.get('month'));
    today = date.today()

    employer = Employer.objects.get(pk=request.GET.get('employer_id'));
    start_date = date(today.year,month,1)
    end_date = date(today.year,month,calendar.mdays[month])

     #hari libur
    holidays  = Holidays.objects.filter(date__month=month,date__year=today.year)
    listDay = [];

    data = []
    for dt in rrule(DAILY,dtstart=start_date,until=end_date):
        row = {}
        d = dt.strftime("%m-%d-%Y")
        
        row['date'] = d
        row['in'] = "-"
        row['out'] ="-"
        
        d = dt.strftime("%d")
        dayName =  dt.strftime("%a")
        
        if dayName == 'Sat' or dayName == 'Sun' :
             row['status'] = "Hari Libur"
        elif  d  in listDay :
             row['status'] = "Hari Libur"
        else:
            y = int(dt.strftime("%Y"))
            m = int(dt.strftime("%m"))
            dy = int(dt.strftime("%-d"))

            scan = ScanLog.objects.filter(pin=employer.pin,scan_date__date=date(y,m,dy)).first()
            
            if scan :
                row['status'] = "Masuk"
                row['in'] = scan.cekin
                row['out'] = scan.cekout
            else :
                izin = AttendanceList.objects.filter(employer_id=employer.id,date__day=dy,date__year=y,date__month=m).first()
               
                if(izin) :
                
                    if izin.status == "I" :
                        row['status'] = "Izin"
                    elif izin.status == "S" :
                        row['status'] = "Sakit"
                    else:
                        row['status'] = "Cuti"
                else :
                    row['status'] = "Tidak Masuk"
        

        data.append(row)


    return render(request,'attendance/user.html',{'employer': employer ,'data' : data });


# generate 

def view_generate_salary(request) :

    if request.method == 'POST':
        today = date.today()
        month = int(request.POST.get('month'))
        emp_id = int(request.POST.get('employer'))
        emp = Employer.objects.filter(pk=emp_id).first()
        start_date = date(today.year,month,1)
        end_date = date(today.year,month,calendar.mdays[month])

        #hari libur
        holidays  = Holidays.objects.filter(date__month=month,date__year=today.year)
        listDay = [];

        for d in holidays :
            day = d.date
            listDay.append(day.strftime("%d"));
        
        #absensi
        masuk = 0
        tidak = 0    
        for dt in rrule(DAILY,dtstart=start_date,until=end_date):
            d = dt.strftime("%d")
            dayName =  dt.strftime("%a")
            
            if dayName == 'Sat' or dayName == 'Sun' :
                pass
            elif  d  in listDay :
                pass
            else:
                y = int(dt.strftime("%Y"))
                m = int(dt.strftime("%m"))
                dy = int(dt.strftime("%-d"))

                data = ScanLog.objects.filter(pin=emp.pin,scan_date__date=date(y,m,dy)).count()
                
                if data!=0 :
                    masuk += 1
                else :
                    tidak += 1

        gaji =  emp.position.salary
        allowance = Allowance.objects.all()

        tunjangan = 0
        for a in allowance :
            total = a.amount * masuk
            tunjangan += total


        denda = 0
        if tidak>0 :
            dendaAbsen = gaji/30
            denda = tidak*dendaAbsen

        penghasilan = gaji+tunjangan-denda
        penghasilanThn = penghasilan*12

        #pph21
        status_pajak = StatusPajak.objects.filter(name=emp.status_pajak).first()
        ptkp = status_pajak.amount
        pkpThn = penghasilanThn-ptkp


        if pkpThn<= 50000000  :
            pph21Thn = pkpThn*0.05
        elif pkpThn>50000000 and pkpThn<= 250000000 :
            pph21Thn = pkpThn*0.15
        elif pkpThn>250000000 and pkpThn<= 500000000 :
            pph21Thn = pkpThn*0.25
        elif pkpThn> 500000000 :
            pph21Thn = pkpThn*0.3
        
        pph21 = pph21Thn/12

        total_deduction = pph21+denda


        slip  = SalarySlip()
        slip.total_income = penghasilan
        slip.total_deduction = total_deduction
        slip.employer = emp
        slip.date = today
        slip.month = month
        slip.year = today.year
        slip.total_pay = penghasilan-total_deduction
        slip.save()

        #income
        i = Income(name="Gaji Pokok",unit=1,total=gaji,salaryslip=slip)
        i.save()

        for a in allowance :
            total = a.amount * masuk
            i = Income(name=a.name,unit=masuk,total=total,salaryslip=slip)
            i.save()

        #pengurangan
        d = Deduction(name="pph 21",unit=1,total=pph21,salaryslip=slip)
        d.save()
        
        if tidak>0 :
            d = Deduction(name="Tidak Masuk",unit=tidak,total=denda,salaryslip=slip)
            d.save()

        return redirect('payroll-detail', pk=slip.id)



    else :

        today = date.today()
        selectedMounth = today.month
        mounth  = [];
        for i in range(1, 13):
            mounth.append(calendar.month_name[i])

        employers = Employer.objects.all()

        return render(request,'payroll/generate.html',{'employers':employers,'mounth':mounth,'selectedMounth':selectedMounth})
    
def Pay(request, pk):
    slip = SalarySlip.objects.get(pk=pk)
    slip.payment_status = True
    slip.save()    
    return redirect('payroll-detail', pk=slip.id)
   
class ViewListPayroll(ListView):

    model = SalarySlip
    paginate_by = 10
    template_name = 'payroll/list.html'


    def get_queryset(self):
        return self.model.objects.filter(month=self.request.GET.get('month'),year=date.today().year)

    def get_context_data(self, **kwargs):
        context = super(ViewListPayroll, self).get_context_data(**kwargs)
        context['month'] = self.request.GET.get('month')
        mounth  = [];
        for i in range(1, 13):
            mounth.append(calendar.month_name[i])
        context['listMonth'] = mounth
        return context


class ViewDetailPayroll(DetailView):
    model = SalarySlip
    template_name = 'payroll/detail.html'



class ViewListAllowance(ListView):
    queryset = Allowance.objects.all()
    paginate_by = 10
    template_name = 'allowance/list.html'


class ViewCreateAllowance(CreateView):
     template_name = 'allowance/create.html'
     model = Allowance
     form_class = AllowanceForm
     success_url = '/allowance'


class ViewUpdateAllowance(UpdateView):

    model = Allowance
    template_name = 'allowance/update.html'
    form_class = AllowanceForm
    success_url = '/allowance'

    def get_object(self, queryset=None):
        id = self.kwargs['id']
        return self.model.objects.get(id=id)

    

class ViewDetailAllowance(DetailView):
    model = Allowance
    template_name = 'allowance/detail.html'


def DeleteAllowance(request, pk):
    allowance = Allowance.objects.get(pk=pk)
    allowance.delete()
    return JsonResponse({'status':'deleted'})


def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html  = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None

class GeneratePdf(View):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        slip = SalarySlip.objects.filter(pk=pk).first()
    
        pdf = render_to_pdf('pdf.html', {'slip':slip})
        return HttpResponse(pdf, content_type='application/pdf')


#izin

class ViewCreateIzin(CreateView):
    template_name = 'izin/create.html'
    model = Izin
    form_class = IzinForm
    success_url = '/izin/employer'

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.employer = Employer.objects.get(pk=self.request.user.employer_id)
        return super(ViewCreateIzin,self).form_valid(form)
    


class ViewListIzinEmployer(ListView):
    model = Izin
    paginate_by = 10
    template_name = 'izin/list-emp.html'

    def get_queryset(self):
        return Izin.objects.filter(employer__pk=self.request.user.employer_id)
    def get_context_data(self, **kwargs):
        context = super(ViewListIzinEmployer, self).get_context_data(**kwargs)
        context['employer_id'] = self.request.user.employer_id
        return context

class ViewListIzin(ListView):
    queryset = Izin.objects.all();
    paginate_by = 10
    template_name = 'izin/list.html'



def izin_approval(request,pk) :

    izin = Izin.objects.get(pk=pk)
    izin.status = True
    izin.save()

    emp = Employer.objects.filter(pk=izin.employer_id).first()

    date = izin.date
    date = date.replace(" ", "")
    rangedate  =  date.split('-') 
    start_date = datetime.strptime(rangedate[0], "%m/%d/%Y")
    end_date = datetime.strptime(rangedate[1], "%m/%d/%Y")

    for dt in rrule(DAILY,dtstart=start_date,until=end_date):
        date = dt.strftime("%Y-%m-%d")
        attendance = AttendanceList(date=date,status=izin.type,employer=emp)
        attendance.save()
   
    return redirect('izin-list')


#cuti 
class ViewListCuti(ListView):
    queryset = JatahCuti.objects.all()
    paginate_by = 10
    template_name = 'cuti/list.html'


class ViewPengajuanCutiEmployer(ListView):
    model = PengajuanCuti
    paginate_by = 10
    template_name = 'cuti/pengajuan-emp.html'

    def get_queryset(self):
        return PengajuanCuti.objects.filter(employer__pk=self.request.user.employer_id)
    def get_context_data(self, **kwargs):
        context = super(ViewPengajuanCutiEmployer, self).get_context_data(**kwargs)
        context['employer_id'] = self.request.user.employer_id
        return context

class ViewPengajuanCuti(ListView):
    queryset = PengajuanCuti.objects.all()
    paginate_by = 10
    template_name = 'cuti/pengajuan.html'


class ViewCreateCuti(CreateView):
    model = PengajuanCuti
    form_class = CutiForm
    template_name =  'cuti/create.html';
    success_url = '/cuti'

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.employer = Employer.objects.get(pk=self.request.user.employer_id)
        return super(ViewCreateCuti,self).form_valid(form)



def cuti_approval(request,pk) :

    cuti = PengajuanCuti.objects.get(pk=pk)   

    jatahCuti = JatahCuti.objects.filter(employer_id=cuti.employer_id).first()

    if jatahCuti.take != jatahCuti.quota :
        emp = Employer.objects.filter(pk=cuti.employer_id).first()
        date = cuti.date
        date = date.replace(" ", "")
        rangedate  =  date.split('-') 
        start_date = datetime.strptime(rangedate[0], "%m/%d/%Y")
        end_date = datetime.strptime(rangedate[1], "%m/%d/%Y")

        for dt in rrule(DAILY,dtstart=start_date,until=end_date):
            date = dt.strftime("%Y-%m-%d")
            attendance = AttendanceList(date=date,status="Cuti",employer=emp)
            attendance.save()

        jatahCuti.take = jatahCuti.take+1
        jatahCuti.save()

        cuti.status = True
        cuti.save()

    else :
        messages.error(request, 'Jatah cuti telah terpakai')
    
    return redirect('cuti-pengajuan')
#login

def login_page(request) :
    form = LoginForm(request.POST or None)
    if form.is_valid() :
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username,password=password)
        if user is not None :
            login(request,user)
            return redirect('/')
        else :
            messages.error(request, 'Username / Password salah')

    return render(request,'login.html',{'form':form})