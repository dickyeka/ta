"""payroll URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from .views import *
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import logout

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',index),
    #emp
    url(r'^employer/?$', ViewListEmployer.as_view(), name='employer-list'),
    url(r'^employer/detail/(?P<pk>\d+)/$',ViewDetailEmployer.as_view(), name='employer-detail'),
    url(r'^employer/update/(?P<id>\d+)/$', ViewUpdateEmployer.as_view(), name='employer-update'),
    url(r'^employer/create/$', ViewCreateEmployer.as_view(), name='employer-create'),
    url(r'^employer/delete/(?P<pk>\d+)/$', DeleteEmploye, name='employer-delete'),

    #job
    url(r'^employer/(?P<class>\d+)/job/create$', CreateViewEmployerJobCreate.as_view(), name='employer-job'),
    url(r'^employer/job/(?P<pk>\d+)/$',ViewListJob.as_view(), name='job-list'),
    url(r'^job/update/(?P<id>\d+)/$', ViewUpdateJob.as_view(), name='job-update'),
    
    #kehadrian
    url(r'^attendance/?$',list_kehadiran, name='attendance-list'),
    url(r'^rekap-kehadiran/?$',rekap_kehadiran, name='rekap-kehadiran'),
    url(r'^attendance/user/?$',user_kehadiran, name='attendance-user'),

    #pengajian 
    url(r'^payroll/generate?$', view_generate_salary, name='payroll-generate'),
    url(r'^payroll/pay/(?P<pk>\d+)/$', Pay, name='payroll-pay'),


    url(r'^payroll/?$', ViewListPayroll.as_view(), name='payroll-list'),
    url(r'^payroll/detail/(?P<pk>\d+)/$',ViewDetailPayroll.as_view(), name='payroll-detail'),
    url(r'^allowance/?$', ViewListAllowance.as_view(), name='employer-list'),
    url(r'^allowance/detail/(?P<pk>\d+)/$',ViewDetailAllowance.as_view(), name='allowance-detail'),
    url(r'^allowance/update/(?P<id>\d+)/$', ViewUpdateAllowance.as_view(), name='allowance-update'),
    url(r'^allowance/create/$', ViewCreateAllowance.as_view(), name='allowance-create'),
    url(r'^allowance/delete/(?P<pk>\d+)/$', DeleteAllowance, name='employer-delete'),
    url(r'^pdf/(?P<pk>\d+)/', GeneratePdf.as_view(), name='pdf'),


    #izin 

    url(r'^izin/create/$', ViewCreateIzin.as_view(), name='izin-create'),
    url(r'^izin/$', ViewListIzin.as_view(), name='izin-list'),
    url(r'^izin/approval/(?P<pk>\d+)/$', izin_approval, name='izin-approval'),
    url(r'^izin/employer/$', ViewListIzinEmployer.as_view(), name='izin-employer'),
     
    #cuti
    url(r'^cuti/?$', ViewListCuti.as_view(), name='cuti-list'),
    url(r'^cuti/pengajuan?$', ViewPengajuanCuti.as_view(), name='cuti-pengajuan'),
    url(r'^cuti/create?$', ViewCreateCuti.as_view(), name='cuti-create'),
    url(r'^cuti/approval/(?P<pk>\d+)/$', cuti_approval, name='cuti-approval'),
    url(r'^cuti/employer/$', ViewPengajuanCutiEmployer.as_view(), name='cuti-employer'),


    #
    url(r'^login/?$',login_page, name='login'),
     url(r'^logout/$', logout, {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout')
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
