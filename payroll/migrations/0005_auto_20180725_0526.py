# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-07-25 05:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('payroll', '0004_auto_20180724_2243'),
    ]

    operations = [
        migrations.AddField(
            model_name='izin',
            name='employer',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='payroll.Employer'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='employer',
            name='status_pajak',
            field=models.CharField(choices=[(b'k0', b'k0'), (b'k1', b'k1'), (b'k2', b'k2'), (b'k3', b'k3'), (b'tk0', b'tk0'), (b'tk1', b'tk1'), (b'tk2', b'tk2'), (b'tk3', b'tk3')], max_length=10),
        ),
    ]
