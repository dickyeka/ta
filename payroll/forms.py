from django import forms
from .models import  Employer,Job,Allowance,Izin,PengajuanCuti
from django.forms import ModelForm


class DateInput(forms.DateInput):
    input_type = 'date'


class EmployerForm(ModelForm):

    class Meta:
        model = Employer
        fields = [
            'name',
            'gender',
            'no_ktp',
            'born_date',
            'bron_place',
            'phone',
            'address',
            'email',
            'last_education',
            'educational_institution',
            'major',
            'no_npwp',
            'status_pajak',
            'no_bank',
            'name_bank',
            'account_name_bank',
            'pin',
            'status_pajak'
        ]

        widgets = {
            'born_date': DateInput()
        }


class JobForm(ModelForm) :

    class Meta :
        model = Job
        fields = [
            'position',
            'salary',
            'start_date'
        ]

        widgets = {
            'start_date': DateInput()
        }


class AllowanceForm(ModelForm) :

    class Meta :
        model = Allowance
        fields = [
            'name',
            'amount',
        ]


class IzinForm(ModelForm) :
    surat = forms.FileField(required=False)

    class Meta :
        model = Izin
        fields = [
            'type',
            'date',
            'keterangan',
            'surat'
        ]


class CutiForm(ModelForm) :
    
    class Meta :
        model = PengajuanCuti
        fields = [
            'date',
            'keterangan',
        ]


class LoginForm(forms.Form) :
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())


